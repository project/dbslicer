<?php
/**
 * @file
 * Utility functions.
 */

/**
 * List of tables we leave out from slicing.
 * TODO: make it more modular
 * @return array of blacklisted tables
 */
function dbslicer_table_blacklist() {
  $blacklist = array(
    'cache',
    'cache_block',
    'cache_filter',
    'cache_form',
    'cache_menu',
    'cache_page',
    'cache_update',
    'flood',
    'batch',
    'actions_aid',
    'sessions',
    'access',
    'semaphore',
    'watchdog',
  );
  return array_flip($blacklist);
}

/**
 * Get table definitions from modules that implement hook_schema_references in
 * their .install file.
 * @return
 *  Array of table definitions.
 */
function dbslicer_get_table_definitions() {
  $table_blacklist = dbslicer_table_blacklist();
  $tables = array();
  module_load_all_includes('install');

  // Invoke hook_schema for all modules.
  foreach (module_implements('schema_references') as $m) {
    $current = module_invoke($m, 'schema');
    $tables = array_merge($tables, $current);
  }
  $tables = array_diff_key($tables, $table_blacklist);
  return $tables;
}

/**
 * Get table references from modules that implement hook_schema_references in
 * their .install file.
 * @return
 *  Array of table references.
 */
function dbslicer_get_table_references() {
  $references = array();
  module_load_all_includes('install');
  foreach (module_implements('schema_references') as $m) {
    $current = module_invoke($m, 'schema_references');
    $references = array_merge($references, $current);
  }
  return $references;
}

/**
 * Convert a DB type to type used in graph.
 * @param string $type
 *  Type in database
 * @param boolean $primary_key
 *  If the column is a primary key or not.
 * @return
 *  Type in graph.
 */
function dbslicer_db_type_to_slicing_type($type, $primary_key = FALSE) {
  switch ($type) {
    case 'text':
    case 'varchar':
      return 'text';
    case 'serial':
      if ($primary_key)
        return 'indexer';

      return 'int';
    default:
      return $type;
  }
}

/**
 * Load a record identifiers of a table from database.
 * @param $table
 * @return
 *  Array of identifiers.
 */
function dbslicer_load_record_identifiers($table) {
  $where = array(
    'table_name' => '^' . $table . '$',
  );
  $ri = dbslicer_db_load('dbslicer_record_identifiers', $where);
  if ($ri) {
    $ri = array_pop($ri);
    $ri = explode(';', $ri['columns']);
    return $ri;
  }
  return FALSE;
}

/**
 * Returns <br /> tags.
 * @param $num
 *  Number of <br /> tags we want.
 * @return
 *  A string containing the <br /> tags.
 */
function dbslicer_newline($num = 1) {
  $content = "";
  for ($i = 0; $i < $num; $i++) {
    $content .= "<br />";
  }
  return $content;
}

/**
 * Check if all tables have record identifiers in the database.
 * @return
 */
function dbslicer_check_record_identifiers() {
  $tables = array_keys(dbslicer_get_table_definitions());
  foreach ($tables as $idx => $t) {
    $tables[$idx] = "'$t'";
  }
  $where = array(
    'IN#table_name' => $tables,
  );
  $ri = dbslicer_db_load('dbslicer_record_identifiers', $where);
  if (count($ri) != count($tables)) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Replace special chars in regexp to normal letters by escaping them.
 * @param string $regexp
 */
function dbslicer_escape_regexp($regexp) {
  $regexp_special = array('[', '\\', '^', '$', '.', '|', '*', '?', '+', '(', ')');
  $regexp_replace = array('\\[', '\\\\', '\\^', '\\$', '\\.', '\\|', '\\*',
                          '\\?', '\\+', '\\(', '\\)');
  return str_replace($regexp_special, $regexp_replace, $regexp);
}