<?php

/**
 * @file
 * Implementations of hook_schema_references() of the core modules.
 */

/**
 * Block module's implementation of dbslicer's hook_schema_references.
 * @return
 *  Array of tables that has foreign columns.
 */
function block_schema_references() {
  $references = array();
  $references['blocks_roles'] = array(
    'module' => array(
      'table'  => 'blocks',
      'column' => 'bid',
    ),
    'delta' => array(
      'table'  => 'blocks',
      'column' => 'delta',
    ),
    'rid' => array(
      'table'  => 'role',
      'column' => 'rid',
    ),
  );
  $references['boxes'] = array(
    'bid' => array(
      'table'  => 'blocks',
      'column' => 'bid',
    ),
    'format' => array(
      'table'  => 'filter_formats',
      'column' => 'format',
    ),
  );
  return $references;
}

/**
 * Comment module's implementation of dbslicer's hook_schema_references
 * @return
 *  Array of tables that has foreign columns.
 */
function comment_schema_references() {
  $references = array();
  $references['comments'] = array(
    'nid' => array(
      'table'  => 'node',
      'column' => 'nid',
    ),
    'uid' => array(
      'table'  => 'users',
      'column' => 'uid',
    ),
    'pid' => array(
      'table'  => 'comments',
      'column' => 'cid',
    ),
    'format' => array(
      'table'  => 'filter_formats',
      'column' => 'format',
    ),
  );
  $references['node_comment_statistics'] = array(
    'nid' => array(
      'table'  => 'node',
      'column' => 'nid',
    ),
  );
  return $references;
}

/**
 * Dblog module's implementation of dbslicer's hook_schema_references
 * @return
 *  Array of tables that has foreign columns.
 */
function dblog_schema_references() {
  $references['watchdog'] = array(
    'uid' => array(
      'table'  => 'users',
      'column' => 'uid',
    ),
  );
  return $references;
}

/**
 * Filter module's implementation of dbslicer's hook_schema_references
 * @return
 *  Array of tables that has foreign columns.
 */
function filter_schema_references() {
  $references = array();
  $references['filters'] = array(
    'format' => array(
      'table'  => 'filter_formats',
      'column' => 'format',
    ),
  );
  return $references;
}

/**
 * Locale module's implementation of dbslicer's hook_schema_references
 * @return
 *  Array of tables that has foreign columns.
 */
function locale_schema_references() {
  $references = array();
  $references['locales_target'] = array(
    'lid' => array(
      'table'  => 'locales_source',
      'column' => 'lid',
    ),
    'plid' => array(
      'table'  => 'locales_source',
      'column' => 'lid',
    ),
    'language' => array(
      'table'  => 'languages',
      'column' => 'language',
    ),
  );
  return $references;
}

/**
 * Menu module's implementation of dbslicer's hook_schema_references
 * @return
 *  Array of tables that has foreign columns.
 */
function menu_schema_references() {
  return array();
}

function node_schema_references() {
  $references = array();
  $references['node'] = array(
    'vid' => array(
      'table'  => 'node_revisions',
      'column' => 'vid',
    ),
    'type' => array(
      'table'  => 'node_type',
      'column' => 'type',
    ),
    'language' => array(
      'table'  => 'languages',
      'column' => 'language',
    ),
    'uid' => array(
      'table'  => 'users',
      'column' => 'uid',
    ),
  );
  $references['node_revisions'] = array(
    'uid' => array(
      'table'  => 'users',
      'column' => 'uid',
    ),
  );
  $references['node_counter'] = array(
    'nid' => array(
      'table'  => 'node',
      'column' => 'nid',
    ),
  );
  $references['node_access'] = array(
    'nid' => array(
      'table'  => 'node',
      'column' => 'nid',
    ),
  );
  return $references;
}

/**
 * System module's implementation of dbslicer's hook_schema_references
 * @return
 *  Array of tables that has foreign columns.
 */
function system_schema_references() {
  $references = array();
  $references['menu_links'] = array(
    'router_path' => array(
      'table'  => 'menu_router',
      'column' => 'path',
    ),
  );
  $references['files'] = array(
    'router_path' => array(
      'table'  => 'users',
      'column' => 'uid',
    ),
  );
  return $references;
}

/**
 * Upload module's implementation of dbslicer's hook_schema_references
 * @return
 *  Array of tables that has foreign columns.
 */
function upload_schema_references() {
  $references = array();
  $references['upload'] = array(
    'fid' => array(
      'table' => 'files',
      'column' => 'fid'
    ),
    'nid' => array(
      'table'  => 'node',
      'column' => 'nid',
    ),
  );
  return $references;
}

/**
 * User module's implementation of dbslicer's hook_schema_references
 * @return
 *  Array of tables that has foreign columns.
 */
function user_schema_references() {
  $references = array();
  $references['history'] = array(
    'nid' => array(
      'table'  => 'node',
      'column' => 'nid',
    ),
    'uid' => array(
      'table'  => 'users',
      'column' => 'uid',
    ),
  );
  $references['authmap'] = array(
    'uid' => array(
      'table'  => 'users',
      'column' => 'uid',
    ),
  );
  $references['users_roles'] = array(
    'uid' => array(
      'table'  => 'users',
      'column' => 'uid',
    ),
    'rid' => array(
      'table'  => 'role',
      'column' => 'rid',
    ),
  );
  $references['permission'] = array(
    'rid' => array(
      'table'  => 'role',
      'column' => 'rid',
    ),
  );
  return $references;
}

/**
 * Load a row from blocks table by the row from block_roles
 * @param array $data
 *  A row from block_roles.
 * @return
 *  The loaded row from blocks.
 */
function blocks_from_blocks_roles_dbslicer_load(array $data) {
  $block = array();
  $where = array(
    'module' => '^' . $data['module'] . '$',
    'delta' => $data['delta'],
  );
  $block = dbslicer_db_load('blocks', $where);
  return $block;
}

/**
 * Load a row from blocks_roles table by the row from blocks
 * @param array $data
 *  A row from blocks.
 * @return
 *  The loaded row from blocks_roles.
 */
function blocks_roles_from_blocks_dbslicer_load($data) {
  $blocks_roles = array();
  $where = array(
    'module' => '^' . $data['module'] . '$',
    'delta' => $data['delta'],
  );
  $blocks_roles = dbslicer_db_load('blocks', $where);
  return $blocks_roles;
}

/**
 * Load a parent row from comments table by the row from comments
 * @param array $data
 *  A row from blocks.
 * @return
 *  The loaded row from comments.
 */
function comments_from_comments_dbslicer_load($data) {
  $comments = array();
  if ($data['pid'] == 0)
    return $comments;

  $where = array(
    'cid' => $data['pid'],
  );
  $comments = dbslicer_db_load('comments', $where);
  return $comments;
}