<?php

/**
 * @file
 * Handle the record identifiers of the tablesin the database.
 */

/**
 * Constant variables for record identifiers form
 */
define("SHOW_ALL_TABLES", 0);
define("SHOW_TABLES_WITHOUT_IDENTIFIERS", 1);

/**
 * MENU_CALLBACK for record identifiers form
 * @param $show
 *  If $show is 'all' we list all tables otherwise we show just tables without
 *  record identifiers.
 */
function dbslicer_select_record_identifiers($show = '') {
  $type = SHOW_TABLES_WITHOUT_IDENTIFIERS;
  if ($show === 'all') {
    $type = SHOW_ALL_TABLES;
  }
  return drupal_get_form('dbslicer_select_record_identifiers_form', $type);
}

/**
 * Set record identifiers form.
 * @param $form_state
 * @param $show
 *  If $show is 'all' we list all tables otherwise we show just tables without
 *  record identifiers.
 */
function dbslicer_select_record_identifiers_form($form_state, $show = SHOW_ALL_TABLES) {
  $form = array();
  $all_tables = dbslicer_get_table_definitions();

  if (SHOW_ALL_TABLES == $show) {
    $form['hide'] = array(
      '#type' => 'markup',
      '#value' => l(t('Show only tables haven\'t got record identifiers'), 'admin/dbslicer/recordidentifiers'),
    );
  }

  $form['tables'] = array(
    '#tree' => TRUE,
  );
  foreach ($all_tables as $table_name => $table_def) {
      $where = array(
        'table_name' => '^' . $table_name . '$',
      );
      $identifiers = dbslicer_db_load('dbslicer_record_identifiers', $where);
      if (SHOW_TABLES_WITHOUT_IDENTIFIERS == $show && count($identifiers) != 0) {
        continue;
      }
      if (count($identifiers) == 0) {
        if (isset($table_def['unique keys'])) {
          $identifiers = array_shift($table_def['unique keys']);
        }
      }
      else {
        $identifiers = array_pop($identifiers);
        $identifiers = explode(';', $identifiers['columns']);
      }
      $form['tables'][$table_name] = array(
        '#type' => 'markup',
        '#value' => $table_def['description'],
        '#tree' => TRUE,
      );
      foreach ($table_def['fields'] as $column_name => $column_def) {
        $default_value = 0;
        if (in_array($column_name, $identifiers)) {
          $default_value = 1;
        }

        $form['tables'][$table_name][$column_name] = array(
          '#type'  => 'checkbox',
          '#tree'  => TRUE,
          '#default_value' => $default_value,
        );
        $desc = array();
        if (in_array($column_name, $table_def['primary key']) !== FALSE) {
          $desc[] = t('This column is in primary key.');
        }
        $desc[] = $column_def['description'];
        $form['tables'][$table_name][$column_name]['desc'] = array(
          '#type'  => 'markup',
          '#value' => implode('<br />', $desc),
        );
        $form['tables'][$table_name][$column_name]['column_type'] = array(
          '#type'  => 'markup',
          '#value' => dbslicer_db_type_to_slicing_type($column_def['type']),
        );
      }
  }
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Send'),
  );

  if (count(element_children($form['tables'])) == 0) {
    drupal_set_message(t('All record identifiers have been set.') . ' ' .
      l(t('Show them!'), 'dbslicer/recordidentifiers/all'));
    return array();
  }

  return $form;
}

/**
 * Submit handler for record identifiers form.
 * @param $form
 * @param $form_state
 */
function dbslicer_select_record_identifiers_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['values']['tables']) as $table) {
    $identifiers = array();
    foreach (array_keys($form_state['values']['tables'][$table]) as $column) {
      if ($form_state['values']['tables'][$table][$column]) {
        $identifiers[] = $column;
      }
    }
    dbslicer_db_delete('dbslicer_record_identifiers', array('table_name' => '^' . $table . '$'));
    if (count($identifiers) > 0) {
      $row = array(
        'table_name' => $table,
        'columns' => implode(';', $identifiers),
      );
      dbslicer_db_save('dbslicer_record_identifiers', $row, INSERT);
    }
  }
}

/**
 * Theme function of record identifiers form.
 * @param $form
 */
function theme_dbslicer_select_record_identifiers_form($form) {
  $content = '';
  if (isset($form['hide'])) {
    $content .= drupal_render($form['hide']);
  }
  $headers = array('', t('Column name'), t('Type'), t('Description'));
  foreach (element_children($form['tables']) as $table) {
    $content .= '<h3>' . t('Datatable') . ': ' . $table . '</h3>';
    $rows = array();
    foreach (element_children($form['tables'][$table]) as $column) {
      $desc = drupal_render($form['tables'][$table][$column]['desc']);
      $type = drupal_render($form['tables'][$table][$column]['column_type']);
      $rows[] = array(
        array('data' => drupal_render($form['tables'][$table][$column]),
              'style' => 'width: 1%;'),
        array('data' => $column, 'style' => 'width: 20%;'),
        array('data' => $type, 'style' => 'width: 10%;'),
        $desc,
      );
    }
    $content .= drupal_render($form['tables'][$table]);
    $content .= theme_table($headers, $rows);
  }
  $content .= drupal_render($form);
  return $content;
}