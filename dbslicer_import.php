<?php

/**a
 * @file
 * Functions to import a slice into the database.
 */

/**
 * Convert a DOMNode to DOM document.
 * @param DOMNode $node
 *  The node we want to insert into a DOM document.
 * @return
 *  DOMDocument An xml document that contains the node.
 */
function dbslicer_xml_node_to_doc(DOMNode $node) {
  $doc = new DOMDocument();
  $node = $doc->importNode($node, TRUE);
  $doc->appendChild($node);
  return $doc;
}

/**
 * Load a row from XML by its hash.
 * @param string $table
 *  Name of tables.
 * @param string $hash
 *  Hash of row.
 * @param DOMDocument $schema_doc
 *  Schema document.
 * @param string $file
 *  Name of file we are processing.
 * @return mixed
 *  DOMDocument created from the row. It we can not find the row returns FALSE.
 */
function dbslicer_import_load_by_hash($table, $hash, $schema_doc, $file) {
  $reader = new XMLReader();
  $reader->open(file_directory_path() . '/dbslicer/' . $file);
  $args = array();
  while ($reader->read()) {
    if (XMLREADER::ELEMENT == $reader->nodeType) {
      if ($reader->localName == 'database')
        $reader->next();
      if ($reader->localName == 'content') {
        while ($reader->read()) { //we are int hre content section
          if (XMLREADER::ELEMENT == $reader->nodeType) {
            if ($reader->localName == $table) {
              while ($reader->read()) { //we are at the rows of table
                if (XMLREADER::ELEMENT == $reader->nodeType) {
                  if ($reader->localName == 'row') {
                    $row_doc = dbslicer_xml_node_to_doc($reader->expand());
                    $row_id_nodes = dbslicer_import_row_identifier_nodes($schema_doc, $row_doc, $table);
                    $row_hash = dbslicer_import_node_list_hash($row_id_nodes);
                    if (!$row_hash)
                      return FALSE;

                    if ($hash == $row_hash) {
                      $reader->close();
                      return $row_doc;
                    }
                    $reader->next();
                  }
                }
              }
            }
            else {
              $reader->next();
            }
          }
        }
      }
    }
  }
  $reader->close();
  return FALSE;
}

/**
 * Make a hash from a row's identifier nodes.
 * We concatenate the XML node's name and its text content and create an
 * md5() hash from this string.
 * @param DOMNodeList $node_list
 *  List of identifiers.
 * @return string
 *  Hash associated with the row.
 */
function dbslicer_import_node_list_hash(DOMNodeList $node_list) {
  $string = "";
  foreach ($node_list as $node) {
    $doc = dbslicer_xml_node_to_doc($node);
    $xpath = new DOMXPath($doc);
    $descendants = $xpath->query("/descendant::*");
    foreach ($descendants as $d) {
      if ($d->childNodes->length == 1) {
        $string .= $d->nodeName;
        $string .= $d->nodeValue;
      }
      else {
        $string .= $d->nodeName;
      }
    }
  }
  if (drupal_strlen($string) == 0) {
    return FALSE;
  }
  return md5($string);
}

/**
 * Get the identifier nodes from a row document.
 * @param DOMDocument $schema_doc
 *  Schema document
 * @param DOMDocument $row_doc
 *  Row document from where we load the identifiers.
 * @param string $table
 *  Name of row's table.
 * @return DOMNodeList
 *  The identifiers row.
 */
function dbslicer_import_row_identifier_nodes($schema_doc, $row_doc, $table) {
  $schema_xpath = new DOMXPath($schema_doc);
  $row_xpath = new DOMXPath($row_doc);

  $identifiers = $schema_xpath->query("/database/table[name='$table']/column[identifier]");
  $id_names = array();
  foreach ($identifiers as $id) {
    $name = $schema_xpath->query("name", $id)->item(0)->textContent;
    $id_names[] = "/row/$name";
  }
  $identifiers = $row_xpath->query(implode(' | ', $id_names));
  return $identifiers;
}

/**
 * Load a row from database by the row document's identifiers.
 * We call this function recursive.
 * @param string $table
 *  Name of row's table.
 * @param DOMNodeList $identifiers
 *  Identifier nodes of row document.
 * @param DOMDocument $schema_doc
 *  Schema document
 * @param DOMDocument $row_doc
 *  Row document.
 * @return mixed
 *  FALSE if we don't find the row in DB, or the row's associative array.
 */
function dbslicer_import_load_from_db($table, $identifiers, $schema_doc, $row_doc) {
  $schema_xpath = new DOMXPath($schema_doc);
  $row_xpath = new DOMXPath($row_doc);
  $where = array();
  foreach ($identifiers as $id) {
    $name = $id->nodeName;
    $isReference = ($id->childNodes->length > 1) ? TRUE : FALSE;
    $type = $schema_xpath->query("/database/table[name='$table']/column[name='$name']/type")->item(0)->textContent;
    if ($isReference) {
      $reference = $row_xpath->query("*/*", $id);
      $ref_table = $row_xpath->query("*", $id)->item(0)->nodeName;

      $ref_data = dbslicer_import_load_from_db($ref_table, $reference, $schema_doc, $row_doc);
      $ref_col = $schema_xpath->query("/database/table[name='$table']/column[name='$name']/reference/@column")->item(0)->textContent;
      $where[$name] = $ref_data[$ref_col];
    }
    else {
      $where[$name] = $id->nodeValue;
    }
    if ($type == 'text') {
      $where[$name] = dbslicer_escape_regexp($where[$name]);
      $where[$name] = '^' . $where[$name] . '$';
    }

  }
  $data = dbslicer_db_load($table, $where);
  $cnt = count($data);
  if ($cnt == 0 || $cnt > 1) {
    $data = FALSE;
  }
  else {
    $data = array_shift($data);
  }
  return $data;
}

/**
 * Process a row document with Batch API.
 * We use a stack to eliminate recursive calls.
 * @param array $process_info
 *  Information we need for processing.
 *    $process_info['table']: name of table where the row is
 *    $process_info['row']: the hash of the row
 * @param string $file
 *  Name of the file we are processing now.
 * @param array $context
 *  Array needed the Batch API.
 */
function dbslicer_import_process_row(array $process_info, $file, &$context) {
  if (isset($context['sandbox']['stack'])) {
    $stack = $context['sandbox']['stack'];
  }
  else {
    $stack = array();
    $item = array();
    $item['process_info'] = $process_info;
    $stack[] = $item;
  }
  $schema_doc = dbslicer_import_get_schema_doc($file);
  $schema_xpath = new DOMXPath($schema_doc);
  // We process 20 item from the stack at once.
  $limit = 20;
  while (count($stack) > 0) {
    $limit--;
    if ($limit == 0) {
      //We reached the limit, save the stack and return.
      $context['sandbox']['stack'] = $stack;
      $context['finished'] = 0;
      return;
    }
    $item = array_shift($stack);
    $table = $item['process_info']['table'];
    $hash = $item['process_info']['row'];
    $row_doc = dbslicer_import_load_by_hash($table, $hash, $schema_doc, $file);
    $row_xpath = new DOMXPath($row_doc);

    $data = array();
    $where = array();
    $columns = $row_xpath->query("/row/*");
    // Process all columns of table
    foreach ($columns as $c) {
      $name = $c->nodeName;
      $isIdentifier = $schema_xpath->query("/database/table[name='$table']/column[name='$name']/identifier")->length;
      $type = $schema_xpath->query("/database/table[name='$table']/column[name='$name']/type")->item(0)->textContent;
      $isReference = ($c->childNodes->length > 1) ? TRUE:FALSE;
      // Process the reference column
      if ($isReference) {
        $reference = $row_xpath->query("*/*", $c);
        $ref_table = $row_xpath->query("*", $c)->item(0)->nodeName;
        $ref_hash = dbslicer_import_node_list_hash($reference);
        if (!$ref_hash) {
            if ($isIdentifier) {
              $context['finished'] = 1;
              return FALSE;
            }
            else {
              continue;
            }
        }
        /*
         * If we have already processed the referenced row load it from the
         * database.
         * If we have found more rows by the referenced rows identifier,
         * we return immediately.
         * Otherwise we put the actual row's and the referenced row's process
         * informations into the stack and continue processing it.
         */
        if (isset($context['results']['result']['found'][$ref_hash])) {
          $ref_data = dbslicer_import_load_from_db($ref_table, $reference, $schema_doc, $row_doc);
        }
        elseif (isset($context['results']['result']['found more'][$ref_hash])) {
          if ($isIdentifier) {
            $context['finished'] = 1;
            return FALSE;
          }
          else {
            continue;
          }
        }
        else {
          $ref_row = dbslicer_import_load_by_hash($ref_table, $ref_hash, $schema_doc, $file);
          if (!$ref_row) {
            //Not found in XML. Try to search in DB.
            $ref_data = dbslicer_import_load_from_db($ref_table, $reference, $schema_doc, $row_doc);
            //Not found in DB or found more with identifiers
            if (!$ref_data) {
              if ($isIdentifier) {
                $context['finished'] = 1;
                return FALSE;
              }
              else {
                continue;
              }
            }
          }
          else {
            $p_info = array(
              'table' => $ref_table,
              'row' => $ref_hash,
            );
            array_unshift($stack, $item);
            $item['process_info'] = $p_info;
            array_unshift($stack, $item);
            continue 2;
          }
        }
        // Get the referenced data from the referenced row.
        $ref_col = $schema_xpath->query("/database/table[name='$table']/column[name='$name']/reference/@column")->item(0)->textContent;
        $data[$name] = $ref_data[$ref_col];
      }
      else {
        $data[$name] = $c->nodeValue;
      }
      if ($isIdentifier) {
        $where[$name] = $data[$name];
        if ($type == 'text') {
          $where[$name] = dbslicer_escape_regexp($where[$name]);
          $where[$name] = '^' . $where[$name] . '$';
        }
      }
    }
    // Load the row from database.
    $row = dbslicer_db_load($table, $where);
    $cnt = count($row);
    $identifiers = dbslicer_import_row_identifier_nodes($schema_doc, $row_doc, $table);
    $row_hash = dbslicer_import_node_list_hash($identifiers);
    if ($cnt == 1) {
      // We found it, compare the data came from database with the data found in XML
      $row_data = array_shift($row);
      $conflict = array();
      foreach ($row_data as $column => $value) {
        if (!array_key_exists($column, $data))
          continue;

        if ($value != $data[$column]) {
          $conflict[] = $column;
        }
      }
      if (count($conflict) > 0) {
        $context['results']['result']['conflict'][$row_hash] = array(
          'table' => $table,
          'columns' => $conflict,
          'db data' => $row_data,
          'xml data' => $data,
        );
      }
      $context['results']['result']['found'][$row_hash] = 1;
      $data = $row_data;
    }
    elseif ($cnt == 0) {
      //we didn't find the row in database. Insert it now.
      dbslicer_db_save($table, $data, INSERT);
      $data = array_shift(dbslicer_db_load($table, $where));
      $context['results']['result']['inserted']++;
    }
    elseif ($cnt > 1) {
      //Found more rows in database by the row's identifiers.
      $context['results']['result']['found more'][$row_hash] = array(
        'table' => $table,
        'identifiers' => $where,
      );
      $data = FALSE;
    }
  }
  $context['finished'] = 1;
  return $data;
}

/**
 * Get the schema doc from the exported row.
 * @param string $file
 *  The file we want to import.
 * @return mixed
 *  DOMDocument schema document, FALSE if we don't find
 */
function dbslicer_import_get_schema_doc($file) {
  $reader = new XMLReader();
  $reader->open(file_directory_path() . '/dbslicer/' . $file);
  while ($reader->read()) {
    switch ($reader->nodeType) {
      case (XMLREADER::ELEMENT):
        if ($reader->localName == 'database') {
          $schema_doc = dbslicer_xml_node_to_doc($reader->expand());
          $reader->close();
          return $schema_doc;
        }
        break;
    }
  }
  return FALSE;
}

/**
 * Import a file.
 * @param string $file
 */
function dbslicer_import($file) {
  $reader = new XMLReader();
  //clear the import table
  dbslicer_db_delete('dbslicer_import_result');

  $reader->open(file_directory_path() . '/dbslicer/' . $file);
  $counter = 0;
  $batch = array();
  while ($reader->read()) {
    switch ($reader->nodeType) {
      case (XMLREADER::ELEMENT):
        if ($reader->localName == 'database') {
          $schema_doc = dbslicer_xml_node_to_doc($reader->expand());
          $reader->next();
        }
        if ($reader->localName == 'content') {
          while ($reader->read()) {
            if ($reader->nodeType == XMLREADER::ELEMENT) {
              if ($reader->localName == 'row') {
                $counter++;
                $row_doc = dbslicer_xml_node_to_doc($reader->expand());
                $identifiers = dbslicer_import_row_identifier_nodes($schema_doc, $row_doc, $table);
                $hash = dbslicer_import_node_list_hash($identifiers);
                $p_info = array(
                  'table' => $table,
                  'row' => $hash,
                );
                /*
                 * We collect the informations into the batch's operation array.
                 */
                $batch['operations'][] = array(
                  'dbslicer_import_process_row',
                  array($p_info, $file),
                );
                $reader->next(); // jump to the next node, skip children.
              }
              else {
                $table = $reader->localName;
              }
            }
          } // table name
        }
        break;
    }
  }
  $reader->close();
  // setting up batch array
  $batch['finished'] = 'dbslicer_import_finished_batch';
  $batch['title'] = t('Processing import file');
  $batch['init_message'] = t('Processing rows.');
  $batch['progress_message'] = t('Processed @current out of @total.');
  $batch['error_message'] = t('Processing import file has encountered an error.');
  $batch['file'] = drupal_get_path('module', 'dbslicer') . '/dbslicer_import.php';
  return $batch;
}

/**
 * Finish function of import batch.
 * @param $success
 * @param $results
 * @param $operations
 */
function dbslicer_import_finished_batch($success, $results, $operations) {
  $result = $results['result'];
  $row = array(
    'result' => serialize($result),
  );
  dbslicer_db_save('dbslicer_import_result', $row, INSERT);
  $iid = db_last_insert_id('dbslicer_import_result', 'iid');
  drupal_set_message(t('Processing import file has been finished.') .' ' .
    l(t('Show'), 'dbslicer/result/' . $iid, array('attributes' => array('style' => 'font-weight: bold;'))) . ' ' . t('the results.'));
}