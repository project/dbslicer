<?php

/**
 * @file
 * Database abstraction layer.
 */

/**
 * Converts database type to a format string.
 * @param $type
 *  Type in DB.
 * @return
 *  Format string.
 */
function dbslicer_db_field_format_string($type) {
  switch ($type) {
    case 'varchar':
    case 'text':
      return "'%s'";
    case 'int':
    case 'serial':
      return '%d';
    case 'blob':
      return '%b';
  }
}

/**
 * Returns a compare sign of a type in DB
 * @param $type
 *  Type in DB.
 * @return
 *  Compare sign.
 */
function dbslicer_db_field_compare_string($type) {
  switch ($type) {
    case 'varchar':
    case 'text':
      return " ~* ";
    case 'int':
    case 'serial':
      return '=';
  }
}

/**
 * Returns a NOT compare sign of a type in DB
 * @param $type
 *  Type in DB.
 * @return
 *  NOT compare sign.
 */
function dbslicer_db_field_compare_not_string($type) {
  switch ($type) {
    case 'varchar':
    case 'text':
      return "!~*";
    case 'int':
    case 'serial':
      return '!=';
  }
}

/**
 * Parsing a where array.
 * @param array $fields
 *  Fields of a table.
 * @param array $where
 *  The array we have to parse.
 *    Rules:
 *      column  => value: a column = value pair will be inserted into query
 *                        The equal operator will be column type specific.
 *      !column => value: a column != value pair will be inserted into query
 *                        The not equal operator will be column type specific.
 *      op#column => value:
 *        op can be: <,>, <=, >=
 *                   IN: SQL statement. value must be an array of data
 *                   op must not start with !
 *      #expr => value: value will be inserted into query as plain text
 * @param $params
 *  We put the data we have to pass to db_query into this array.
 * @return
 *  The query string contains the WHERE part of the full query.
 */
function dbslicer_db_parse_where(array $fields, array $where = array(), &$params) {
  $q = '';
  $format = array();
  foreach ($where as $field => $value) {
    $pos = strpos($field, '#');
    if ($pos) {
      $op = drupal_substr($field, 0, $pos);
      $field = drupal_substr($field, $pos + 1);
    }
    else {
      if (drupal_substr($field, 0, 1) == '!') {
        $field = drupal_substr($field, 1);
        $op = dbslicer_db_field_compare_not_string($fields[$field]['type']);
      }
      else {
        $op = dbslicer_db_field_compare_string($fields[$field]['type']);
      }
    }
    if ($op == 'IN') {
      $format[] = sprintf('%s IN (%s)', $field, implode(',', $value));
    }
    elseif ($field == '#expr') {
      if (is_array($value)) {
        foreach ($value as $v) {
          $format[] = $v;
        }
      }
      else {
        $format[] = $value;
      }
    }
    else {
      if (is_array($value)) {
        foreach ($value as $v) {
          $format[] =
            sprintf('%s %s %s', $field, $op,
                      dbslicer_db_field_format_string($fields[$field]['type']));
          $params[] = $v;
        }
      }
      else {
        $format[] =
          sprintf('%s %s %s', $field, $op,
                      dbslicer_db_field_format_string($fields[$field]['type']));
        $params[] = $value;
      }
    }
  }
  if (count($format) > 0) {
    $q .= " WHERE ";
    $q .= implode(' AND ', $format);
  }
  return $q;
}

/**
 * Load data from a database table.
 * @param string $table
 *  Table name.
 * @param array $where
 *  An array describes the WHERE part of the query.
 *  Every item will be AND-ed together.
 * @param $order_field
 *  Array of fields we want to order the result by.
 * @param $debug
 *  Boolean value. If TRUE the query will be printed out.
 * @return
 *  An array of the found rows.
 *  Every row's keys will be the columns of table.
 */
function dbslicer_db_load($table, array $where = array(), array $order_field = array(), $debug = FALSE) {
  $table_definitions = drupal_get_schema();
  $fields = $table_definitions[$table]['fields'];
  $q = sprintf("SELECT * FROM {%s}", $table);
  $params = array();

  $q .= dbslicer_db_parse_where($fields, $where, $params);
  if (count($order_field) > 0) {
    $q .= " ORDER BY ";
    $q .= implode(',', $order_field);
  }

  array_unshift($params, $q);
  $qr = call_user_func_array('db_query', $params);

  if ($debug) {
    drupal_set_message(call_user_func_array('sprintf', $params));
  }
  if (!$qr) {
    return FALSE;
  }
  $result = array();
  $i = 0;
  while ($o = db_fetch_object($qr)) {
    foreach (array_keys($fields) as $name) {
      $result[$i][$name] = $o->$name;
    }
    $i++;
  }
  return $result;
}

/**
 * Type of inserting into database. Used when dbslicer_db_save called.
 * @var int
 */
define("INSERT", 1);
/**
 * Type of updating database. Used when dbslicer_db_save called.
 * @var int
 */
define("UPDATE", 2);

/**
 * Saving or updating a row into the database.
 * @param $table
 *  Name of the table.
 * @param array $row
 *  The row's data we want to insert or update.
 * @param $type
 *  Type of saving. Could be INSERT or UPDATE
 * @param $where
 *  An array describes the WHERE part of the query. Used when updating.
 * @return
 *  Returns the db_query result.
 */
function dbslicer_db_save($table, array $row, $type, array $where = array()) {
  $table_definitions = drupal_get_schema();
  $fields = $table_definitions[$table]['fields'];
  $params = array();
  switch ($type) {
    case INSERT: {
      $columns = array();
      $format_strings = array();
      foreach ($row as $field => $value) {
        $columns[] = $field;
        $format_strings[] = dbslicer_db_field_format_string($fields[$field]['type']);
        $params[] = $value;
      }
      $q  = sprintf("INSERT INTO {%s} ", $table);
      $q .= "(" . implode(',', $columns) . ") ";
      $q .= "VALUES ";
      $q .= "(" . implode(',', $format_strings) . ")";
      array_unshift($params, $q);
    }
    break;
    case UPDATE: {
      if (count($where) == 0)
        return FALSE;

      $q = sprintf("UPDATE {%s} SET ", $table);
      $set = array();
      foreach ($row as $field => $value) {
        $set[] = sprintf("%s=%s", $field, dbslicer_db_field_format_string($fields[$field]['type']));
        $params[] = $value;
      }

      $q .= implode(',', $set);
      $q .= dbslicer_db_parse_where($fields, $where, $params);
      array_unshift($params, $q);
    }
    break;
  }
  $qr = call_user_func_array('db_query', $params);
  return $qr;
}

/**
 * Delete a row from database.
 * @param $table
 *  Name of the table.
 * @param $where
 *  An array describes the WHERE part of the query.
 * @return
 *  TRUE if deletion was successfull, FALSE otherwise.
 */
function dbslicer_db_delete($table, array $where = array()) {
  $params = array();
  $table_definitions = drupal_get_schema();
  $fields = $table_definitions[$table]['fields'];

  $q = sprintf("DELETE FROM {%s} ", $table);
  $q .= dbslicer_db_parse_where($fields, $where, $params);
  array_unshift($params, $q);
  $qr = call_user_func_array('db_query', $params);
  if (!$qr) {
    return FALSE;
  }
  return TRUE;
}
