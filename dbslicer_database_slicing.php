<?php

/**
 * @file
 * Algorithms for database slicing.
 */

include_once 'dbslicer_export.php';

/**
 * Generates the Table Dependence Graph from modules' implementation of
 * hook_schema() and hook_schema_references() in Drupal.
 * TDG structure:
 * TDG[tables]: array of tables
 * TDG[columns]: Array of columns in tables.
 *               Column names will be tablename:columname.
 * TDG[primary keys]: Array of primary keys of tables.
 *                     The columns' name in a key will be tablename:columnname.
 *                     Array key will be tablename.
 * TDG[record identifiers]: Array of minimal superkeys of tables.
 *                           The columns' name in a key will be
 *                           tablename:columnname. Array key will be tablename.
 * TDG[adj]: adjacency list of graph
 * TDG[type map]: Types of columns.
 *                These types aren't the same as types in database.
 *                They are mapped to a simpler type.
 *                The rules of mapping are defined by the Database Slicing Method.
 *                Array keys will be tablename:columnname
 * @return
 *  TDG of database
 */
function dbslicer_generate_TDG() {
  $tables = dbslicer_get_table_definitions();
  $table_references = dbslicer_get_table_references();
  $TDG = array();
  $TDG['tables'] = array();
  $TDG['columns'] = array();
  $TDG['primary keys'] = array();
  $TDG['record identifiers'] = array();
  $TDG['type map'] = array();
  $TDG['adj'] = array();
  foreach ($tables as $table_name => $t) {
    $TDG['tables'][] = $table_name;
    $TDG['adj'][$table_name] = array();
    $primary_key = array();
    foreach ($t['primary key'] as $key) {
      $primary_key[] = $table_name . ':' . $key;
    }
    $TDG['primary keys'][$table_name] = $primary_key;
    $record_identifiers = dbslicer_load_record_identifiers($table_name);
    $TDG['record identifiers'][$table_name] = $record_identifiers;

    foreach ($t['fields'] as $column_name => $c) {
      $name = $table_name . ':' . $column_name;
      $TDG['columns'][] = $name;
      $TDG['adj'][$table_name][] = $name;
      $TDG['type map'][$name] = dbslicer_db_type_to_slicing_type($c['type'], in_array($name, $TDG['primary keys'][$table_name]));
    }
    if (isset($table_references[$table_name])) {
      foreach ($table_references[$table_name] as $column_name => $reference) {
        $name = $table_name . ':' . $column_name;
        $foreign_column = $reference['table'] . ':' . $reference['column'];
        $TDG['adj'][$name][] = array($reference['table'], $foreign_column);
      }
    }
  }
  return $TDG;
}

/**
 * Generates a Record Dependence Graph by the module's implementation of
 * hook_schema() and hook_schema_references().
 * RDG structure:
 *  By definition it has to include all rows from database. We can't do this,
 *  because this needs a lots of memory. So we make same structure as in TDG and
 *  we will load the needed rows from database when it is necessary.
 *  TDG edge set called 'adj' has been renamed to 'connections'.
 *  In the tables array we store the record identifiers of each loaded rows of
 *  all tables.
 */
function dbslicer_generate_RDG() {
  $tables = dbslicer_get_table_definitions();
  $table_references = dbslicer_get_table_references();
  $RDG = array();
  $RDG['tables'] = array();
  $RDG['columns'] = array();
  $RDG['primary keys'] = array();
  $RDG['record identifiers'] = array();
  $RDG['type map'] = array();
  $RDG['connections'] = array();
  foreach ($tables as $table_name => $t) {
    $RDG['tables'][$table_name] = array();
    $RDG['tables'][$table_name]['max row id'] = 1;
    $RDG['tables'][$table_name]['rows'] = array();
    $RDG['connections'][$table_name] = array();
    $primary_key = array();
    foreach ($t['primary key'] as $key) {
      $primary_key[] = $table_name . ':' . $key;
    }
    $RDG['primary keys'][$table_name] = $primary_key;
    $record_identifiers = dbslicer_load_record_identifiers($table_name);
    $RDG['record identifiers'][$table_name] = $record_identifiers;
    foreach ($t['fields'] as $column_name => $c) {
      $name = $table_name . ':' . $column_name;
      $RDG['columns'][] = $name;
      $RDG['connections'][$table_name][] = $name;
      $RDG['type map'][$name] = dbslicer_db_type_to_slicing_type($c['type'], in_array($name, $RDG['primary keys'][$table_name]));
    }
    if (isset($table_references[$table_name])) {
      foreach (element_children($table_references[$table_name]) as $column_name) {
        $name = $table_name . ':' . $column_name;
        $foreign_column = $table_references[$table_name][$column_name]['table'] .
                     ':' . $table_references[$table_name][$column_name]['column'];
        $RDG['connections'][$name][] = $foreign_column;
      }
    }
  }
  return $RDG;
}

/**
 * Search for the table of the column or row.
 * @param $data
 *  Searching for table of this column or row.
 * @param array $G
 *  The grap we search in.
 * @return
 *  The table if found, FALSE otherwise
 */
function dbslicer_find_table($data, array $G) {
  foreach ($G['tables'] as $table) {
    if (in_array($data, $G['adj'][$table])) {
      return $table;
    }
  }
  return FALSE;
}

/*
 * Colors used in slicing algorithms.
 */
define("WHITE", 0);
define("GREY",  1);
define("BLACK", 2);

/**
 * Slicing method for static slicing.
 * @param array $TDG
 *  Table Dependence Graph of database.
 * @param string $SPS
 *  Starting Point of Slicing. A table name.
 * @return
 *  Result array of tables.
 */
function dbslicer_slice(array &$TDG, $SPS, $distance = -1) {
  /* Set of vertices */
  $V = array_merge($TDG['tables'], $TDG['columns']);
  /* Working queue. */
  $Q = array();
  /* Stores the color of each node. */
  $color = array();
  /* Stores the results of slicing */
  $result = array();
  /* Stores the distance from the starting point */
  $d = array();
  /* Set up the default colors. */
  foreach ($V as $u) {
    $color[$u] = WHITE;
  }
  $d[$SPS] = 0;
  $color[$SPS] = GREY;
  $Q[] = $SPS;
  while (count($Q) > 0) {
    $u = array_shift($Q);
    if (isset($TDG['adj'][$u])) {
      foreach ($TDG['adj'][$u] as $v) {
        if (is_array($v)) {
          $v = $v[0];
        }
        if (WHITE == $color[$v]) {
          $color[$v] = GREY;
          $d[$v] = $d[$u] + 1;
          if ($distance == -1 || $d[$v] <= $distance * 2) {
            $Q[] = $v;
          }
        }
      }
    }
    $color[$u] = BLACK;
    // If the node is in the tables array, we save it in our result set
    if (in_array($u, $TDG['tables']) !== FALSE) {
      $result[] = $u;
    }
  }
  return $result;
}

/**
 * Prepare a Table Dependence Graph for Backward+Forward Slicing
 * Remove the edge's orientation between foreign columns and referenced tables
 * and between owner tables.
 * @param array $TDG
 *  Table Dependence Grap we want to prepare.
 * @return
 *  The prepared TDG.
 */
function dbslicer_prepare_TDG(array $TDG) {
  $G = $TDG;
  foreach ($G['columns'] as $c) {
    if (isset($G['adj'][$c])) {
      $t = $G['adj'][$c][0][0];
      $TDG['adj'][$t][] = $c;
      $u = dbslicer_find_table($c, $G);
      $TDG['adj'][$c][] = $u;
    }
  }
  return $TDG;
}

/* Directions of Slicing */
define("FS",  0);
define("BFS", 1);

/**
 * Perform a static slice on TDG.
 * @param array $TDG
 *  Table Dependence Graph
 * @param string $SPS
 *  Starting Point of Slicing. A name of table.
 * @param int $direction
 *  Direction of slicing. Could be FS (Forward Slicing) or BFS (Backward+Forward Slicing)
 * @param int $distance
 *  Distance limit of slicing.
 * @return
 *  Result of slicing.
 */
function dbslicer_static_slice(array $TDG, $SPS, $direction, $distance = -1) {
  if (BFS == $direction)
    $TDG = dbslicer_prepare_TDG($TDG);

  return dbslicer_slice($TDG, $SPS, $distance);
}

/**
 * Search for a row's unique name loaded from table.
 * @param array $RDG
 *  Record Dependence Graph
 * @param string $table
 *  Name of table we search in
 * @param array $data
 *  The row's data in an associative array.
 * @return
 *  Returns the row name if it was loaded before or FALSE otherwise.
 */
function dbslicer_get_row_name(array $RDG, $table, array $data) {
  $record_identifiers = array();
  foreach ($RDG['record identifiers'][$table] as $id) {
    $record_identifiers[] = $data[$id];
  }
  $row_name = array_search($record_identifiers, $RDG['tables'][$table]['rows']);
  return $row_name;
}

/**
 * Sets a rows unique name loaded from table.
 * The name will be: table_name:number
 * @param array $RDG
 *  Record Dependence Graph
 * @param string $table
 *  Name of tables.
 * @param array $data
 *  Associative array of the loaded row.
 * @return
 *  The unique name.
 */
function dbslicer_set_row_name(array &$RDG, $table, array $data) {
  $record_identifiers = array();
  foreach ($RDG['record identifiers'][$table] as $id) {
    $record_identifiers[] = $data[$id];
  }
  $row_name = $table . ':' . $RDG['tables'][$table]['max row id']++;
  $RDG['tables'][$table]['rows'][$row_name] = $record_identifiers;

  return $row_name;
}

/**
 * Prepare a Record Dependence Graph for Backward+Forward Slicing.
 * Remove the oreientation of edges between the row nodes.
 * @param array $RDG
 *  Record Dependence Graph
 * @return
 *  Prepared graph.
 */
function dbslicer_prepare_RDG(array $RDG) {
  $G = $RDG;
  foreach ($G['columns'] as $c) {
    if (isset($G['connections'][$c])) {
      $fk = $G['connections'][$c][0];
      $RDG['connections'][$fk][] = $c;
    }
  }
  return $RDG;
}

/**
 * Get the table's name from a column or row name.
 * @param $name
 *  Name of the column.
 * @return
 *  The table name.
 */
function dbslicer_ntot($name) {
  return array_shift(explode(':', $name));
}

/**
 * Get the column's name in DB from a table_name:column:name pair.
 * @param $name
 *  Name of the column in graph.
 * @return
 *  The column name.
 */
function dbslicer_ntoc($name) {
  return array_pop(explode(':', $name));
}

/**
 * Slicing for dynamic slicing method.
 * We load rows from database when they are needed.
 * @param array $RDG
 *  Record Dependence Graph
 * @param array $SPS
 *  Starting Point of Slicing
 *  Structure of SPS:
 *    [table]: table name
 *    [column]: Array of data. Key is column name.
 * @param int $distance
 *  Distance limit of slicing. Negative value means there is no limit.
 * @param array $RDG_orig
 *  Original RDG without prepare.
 * @param array $context
 *  The working array for Batch API.
 *  The result will be in $context['results']['result'].
 */
function dbslicer_dslice(array $RDG, array $SPS, $distance = -1, array $RDG_orig, &$context) {
  module_load_all_includes('install');

  if (!isset($context['sandbox']['queue'])) {
    /* Working queue. */
    $Q = array();
    /* Stores the color of each node. */
    $color = array();
    /* Stores the results of slicing */
    $result = array();
    /* Stores the distance from the starting point */
    $dist = array();
    /* Load the data */
    $data = dbslicer_db_load($SPS['table'], $SPS['column']);
    if (!$data || count($data) == 0) {
      /* We don't find the starting point. Can't start slicing */
      drupal_set_message(t("We don't find the starting point. Can't start slicing"), 'error');
      $context['finished'] = 1;
      return FALSE;
    }
    if (count($data) > 1) {
      /* We find two stating point. Can't start slicing. */
      drupal_set_message(t("We find two starting point. Can't start slicing."), 'error');
      $context['finished'] = 1;
      return FALSE;
    }
    $data = array_pop($data);
    $row_name = dbslicer_set_row_name($RDG, $SPS['table'], $data);
    $result[$row_name] = $data;
    $RDG['adj'][$SPS['table']][] = $row_name;

    $color[$row_name] = GREY;
    $dist[$row_name] = 0;
    $Q[] = $row_name;
  }
  else {
    $Q = $context['sandbox']['queue'];
    $color = $context['sandbox']['color'];
    $result = $context['results']['result'];
    $RDG_orig = $context['results']['RDG'];
    $dist = $context['sandbox']['dist'];
  }
  // We process 20 element from the queue
  $limit = 20;
  while (count($Q) > 0) {
    if ($limit-- == 0) {
      /*
       * We reached the limit, return to Batch API, itt will call the method
       * again. We save the variables we need in the next call.
       */
      $context['sandbox']['queue'] = $Q;
      $context['sandbox']['color'] = $color;
      $context['results']['result'] = $result;
      $context['sandbox']['dist'] = $dist;
      $context['results']['RDG'] = $RDG_orig;
      $context['finished'] = 0;
      return;
    }
    $u = array_shift($Q); //Get an element
    $table = dbslicer_ntot($u);

    //Visit all columns of row
    foreach ($RDG['connections'][$table] as $column) {
      /* If a column is a foreign key, the row has an edge pointed at the
       * referenced row. We load the referenced row, check its color, if white,
       * we put its name into the queue.
       */
      if (isset($RDG['connections'][$column])) {
        foreach ($RDG['connections'][$column] as $fk) {
          $foreign_table = dbslicer_ntot($fk);
          $foreign_column = dbslicer_ntoc($fk);
          /* If a loader function exists between the actual and referenced table
           * we use it to load data.
           */
          if (function_exists($foreign_table . '_from_' . $table . '_dbslicer_load')) {
            $data = call_user_func($foreign_table . '_from_' . $table . '_dbslicer_load', $result[$u]);
          }
          else {
            $where = array();
            if ($RDG['type map'][$fk] == 'text') {
              $where[$foreign_column] = '^' . $result[$u][dbslicer_ntoc($column)] . '$';
            }
            else {
              $where[$foreign_column] = $result[$u][dbslicer_ntoc($column)];
            }
            $data = dbslicer_db_load($foreign_table, $where);
          }
          //we don't found the referenced data, go to next column
          if (!$data || count($data) == 0) {
            continue;
          }
          //process every rows we found
          foreach ($data as $d) {
            $v = dbslicer_get_row_name($RDG, $foreign_table, $d);
            if (!$v) {
              $v = dbslicer_set_row_name($RDG, $foreign_table, $d);
            }

            if (WHITE == $color[$v]) {
              $color[$v] = GREY;
              $dist[$v] = $dist[$u] + 1;
              if ($distance == -1 || $dist[$v] <= $distance) {
                $RDG['adj'][$foreign_table][] = $v;
                $RDG['adj'][$u][] = $v;
                $result[$v] = $d;
                $Q[] = $v;
              }
            }
          }
        }
      }
    }
    $color[$u] = BLACK;
  }
  /* Finished processing, save the result. We need RDG_orig to generate schema
   * document.
   */
  $context['finished'] = 1;
  $context['results']['RDG'] = $RDG_orig;
  $context['results']['result'] = $result;
}

/**
 * Dynamic slicing based on Record Dependence Graph.
 * We set a batch operation. The slicing algorithm will be run in this batch.
 * We will save the results in dbslicer_dynamic_slice_finished_batch() function.
 * @param $RDG
 *  Record Dependence Graph
 * @param $SPS
 *  Starting Point of Slicing
 * @param $direction
 *  Direction of slicing. Could be FS, or BFS.
 * @param $distance
 *  Distance limit of slicing. Negative value means there is no limit.
 */
function dbslicer_dynamic_slice(array $RDG, $SPS, $direction, $distance = -1) {
  $RDG_orig = $RDG;
  if (BFS == $direction)
    $RDG = dbslicer_prepare_RDG($RDG);

  /*
   * Setting up batch.
   */

  $batch['operations'][] = array(
    'dbslicer_dslice',
    array($RDG, $SPS, $distance, $RDG_orig),
  );
  $batch['finished'] = 'dbslicer_dynamic_slice_finished_batch';
  $batch['title'] = t('Slicing database');
  $batch['init_message'] = t('Slicing database...');
  $batch['error_message'] = t('Slicing database has encountered an error.');
  $batch['file'] = drupal_get_path('module', 'dbslicer') . '/dbslicer_export.php';
  batch_set($batch);

  batch_process('dbslicer/manage');
}

/**
 * Finishing operation of batch defined in slicing.
 * We save the results here.
 * @param $success
 * @param $results
 * @param $operations
 */
function dbslicer_dynamic_slice_finished_batch($success, $results, $operations) {
  if ($success) {
    $result = $results['result'];
    $RDG = $results['RDG'];
    $link_to_file = dbslicer_export_dynamic($RDG, $result);
    drupal_set_message(t('Database slice generated:'));
    drupal_set_message(t('You can download it !here.', array('!here' => l(t('here'), $link_to_file))));
  }
  else {
    drupal_set_message(t('Something went wrong while slicing.'), 'error');
  }
}