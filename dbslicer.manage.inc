<?php

/**
 * @file
 * Forms to manage and create database slices.
 */

include_once 'dbslicer_upload.php';
include_once 'dbslicer_import.php';

/**
 * Form to manage exported files and select a file to import.
 * @param unknown_type $form_state
 */
function dbslicer_manage_form($form_state) {
  $form['file'] = array(
    '#tree' => TRUE,
  );
  $cnt = 0;
  foreach (file_scan_directory(file_directory_path() . '/dbslicer', ".*") as $file) {
    $form['file'][$file->basename]['select'] = array(
      '#type' => 'checkbox',
    );
    $form['file'][$file->basename]['link'] = array(
      '#type' => 'markup',
      '#value' => l($file->basename, $file->filename),
    );
    $form['file'][$file->basename]['path'] = array(
      '#type' => 'value',
      '#value' => $file->filename,
    );
    $cnt++;
  }
  ksort($form['file']);
  $form['buttons'] = array();
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  $form['buttons']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#weight' => 15,
    '#submit' => array('dbslicer_manage_delete_submit'),
  );
  if ($cnt == 0) {
    $form = array();
    $form['nofiles'] = array(
      '#type' => 'markup',
      '#value' => t("There aren't any files to import. Create a slice or upload a file."),
    );
  }
  return $form;
}

/**
 * Validate handler for manage form.
 * @param $form
 * @param $form_state
 */
function dbslicer_manage_form_validate($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Import')) {
    $cnt = 0;
    foreach (element_children($form_state['values']['file']) as $file) {
      if ($form_state['values']['file'][$file]['select']) {
        $cnt++;
      }
    }
    if ($cnt != 1) {
      form_set_error('', t('Zero or more than one file have been selected to import. Please select just one.'));
    }
  }
}

/**
 * Submit handler for manage form.
 * @param $form
 * @param $form_state
 */
function dbslicer_manage_form_submit($form, &$form_state) {
  if (!dbslicer_check_record_identifiers()) {
    drupal_set_message(t("Some table's record identifiers are missing! You must set them first!"), 'error');
    drupal_goto('dbslicer/recordidentifiers');
  }
  foreach (element_children($form_state['values']['file']) as $file) {
    if ($form_state['values']['file'][$file]['select']) {
      $file_to_import =  $file;
      break;
    }
  }
  $result = array();
  $batch = dbslicer_import($file_to_import);
  batch_set($batch);
}

/**
 * Submit handler for delete button in manage form.
 * @param $form
 * @param $form_state
 */
function dbslicer_manage_delete_submit($form, &$form_state) {
  $cnt = 0;
  foreach (element_children($form_state['values']['file']) as $file) {
    if ($form_state['values']['file'][$file]['select']) {
      file_delete($form_state['values']['file'][$file]['path']);
      $cnt++;
    }
  }
  drupal_set_message($cnt .' ' . t('file has been removed.'));
}

/**
 * Theming function for manage form.
 * @param $form
 */
function theme_dbslicer_manage_form($form) {
  $headers = array('', t('File'));
  $rows = array();
  $row = array();
  foreach (element_children($form['file']) as $file) {
    $row['data'] = array();
    $row['data'][] = drupal_render($form['file'][$file]['select']);
    $row['data'][] = drupal_render($form['file'][$file]['link']);
    $rows[] = $row;
  }
  if (count($rows) == 0) {
    return  FALSE;
  }
  $output  = theme_table($headers, $rows);
  $output .= drupal_render($form['submit']);
  $output .= drupal_render($form);
  return $output;
}

/**
 * MENU_CALLBACK for manage exported files.
 */
function dbslicer_manage_import() {
  $content = '';
  $content .= '<h3>' . l(t('Start a new slice'), 'dbslicer/slice') . '</h3>';
  $content .= dbslicer_newline(1);
  $content .= '<h3>' . t('Files to import') . '</h3>';
  $content .= drupal_get_form('dbslicer_manage_form');
  $content .= '<h3>' . t('Upload a new file') . '</h3>';
  $content .= drupal_get_form('dbslicer_upload_form');
  return $content;
}

/**
 * Process the import's result.
 * @param $form_state
 * @param $result
 */
function dbslicer_import_result_form($form_state, $result) {
  $form = array();
  if (isset($result['found'])) {
    $form['found'] = array(
      '#tpye' => 'markup',
      '#prefix' => '<div>',
      '#value' => '<h3>' . t('Number of rows have been found in database and XML (includes rows with conflict): ') . count($result['found']) . '</h3>',
      '#suffix' => '</div>',
    );
  }
  if (isset($result['inserted'])) {
    $form['inserted'] = array(
      '#tpye' => 'markup',
      '#prefix' => '<div>',
      '#value' => '<h3>' . t('Number of inserted rows:') . $result['inserted'] . '</h3>',
      '#suffix' => '</div>',
    );
  }
  if (isset($result['id not found'])) {
    $form['not_found'] = array(
      '#type' => 'markup',
      '#value' => '<h3>' . t('Not found referenced identifiers:') . '</h3>',
      '#tree' => TRUE,
    );
    foreach ($result['id not found'] as $not_found) {
      $headers = array();
      $rows = array();
      $row = array();
      foreach ($not_found['identifiers'] as $column => $data) {
        $headers[] = $column;
        $row['data'][] = $data;
      }
      $rows[] = $row;
      $form['not_found'][] = array(
        '#type' => 'markup',
        '#prefix' => t('Table:') . $not_found['table'] . '<br />',
        '#value' => theme_table($headers, $rows),
      );
    }
  }
  if (isset($result['found more'])) {
    $form['found_more'] = array(
      '#type' => 'markup',
      '#value' => '<h3>' . t('Found more than one row by identifiers:') . '</h3>',
      '#tree' => TRUE,
    );
    foreach ($result['found more'] as $found_more) {
      $headers = array();
      $rows = array();
      $row = array();
      foreach ($found_more['identifiers'] as $column => $data) {
        $headers[] = $column;
        $row['data'][] = $data;
      }
      $rows[] = $row;
      $form['found_more'][] = array(
        '#type' => 'markup',
        '#prefix' => t('Table:') . $found_more['table'] . '<br />',
        '#value' => theme_table($headers, $rows),
      );
    }
  }
  if (isset($result['conflict'])) {
    $form['conflicted'] = array(
      '#type' => 'markup',
      '#value' => '<h3>' . t('Rows with conflict:') . '</h3>',
      '#prefix' => '<div style="width:100%;overflow:auto;">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    );
    $conflicts = array();
    $i = 0;
    foreach ($result['conflict'] as $c) {
      $conflicts[$i] = $c['db data'];
      $conflicts[$i + 1] = $c['xml data'];
      $headers = array();
      $row_db = array();
      $row_xml = array();
      foreach ($conflicts[$i] as $column => $data) {
        if (in_array($column, $c['columns'])) {
          $headers[] = array('data' => $column, 'style' => 'background-color:#FFCCCC;');

        }
        else {
          $headers[] = $column;
        }
        $row_db_data = $data;
        $row_xml_data = $conflicts[$i + 1][$column];
        if (!is_numeric($row_dbdata) && drupal_strlen($row_db_data) != 0 &&
            drupal_strlen($row_db_data) > 10) {
          $title_db = $row_db_data;
          $row_db_data = drupal_substr($row_db_data, 0, 10) . '...';
          $row_db_data = array('data' => $row_db_data, 'title' => $title_db);
          $title_xml = $row_xml_data;
          $row_xml_data = drupal_substr($row_xml_data, 0, 10) . '...';
          $row_xml_data = array('data' => $row_xml_data, 'title' => $title_xml);
        }
        $row_db['data'][] = $row_db_data;
        $row_xml['data'][] = $row_xml_data;
      }
      if (!isset($form['conflicted'][$c['table']])) {
        $form['conflicted'][$c['table']] = array(
          '#tree' => TRUE,
        );
      }
      $form['conflicted'][$c['table']][] = array(
        '#type' => 'radios',
        '#prefix' => $c['table'] . ':',
        '#options' => array(
          $i => theme_table($headers, array($row_db)),
          $i + 1 => theme_table($headers, array($row_xml)) . '<hr />',
        ),
        '#default_value' => $i,
        '#attributes' => array('style' => 'float:left;'),
      );
      $i += 2;
    }
    $form['#conflicts'] = $conflicts;
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Update selected rows'),
    );
  }

  return $form;
}

/**
 * Submit handler of result form.
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function dbslicer_import_result_form_submit($form, &$form_state) {
  if (isset($form['#conflicts'])) {
    $conflicts = $form['#conflicts'];
    foreach (element_children($form_state['values']['conflicted']) as $table) {
      foreach ($form_state['values']['conflicted'][$table] as $idx) {
        if (($idx % 2) == 1) {
          dbslicer_db_save($table, $conflicts[$idx], UPDATE, $conflicts[$idx - 1]);
        }
      }
    }
  }
  dbslicer_db_delete('dbslicer_import_result');
  drupal_set_message(t('Selected rows update successfully.'));
  drupal_goto('dbslicer/manage');
}

/**
 * Loads the import result form.
 * @param array $result
 */
function dbslicer_import_result($result) {
  $content = '';
  $content .= drupal_get_form('dbslicer_import_result_form', $result);
  return $content;
}