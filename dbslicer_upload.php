<?php

/**
 * @file
 * Handles file uploads.
 */

/**
 * Upload file form.
 * @param $form_state
 */
function dbslicer_upload_form($form_state) {
  $form['#attributes'] = array(
    'enctype' => "multipart/form-data"
  );
  $form['file_upload'] = array(
    '#type' => 'file',
    '#title' => 'Fájl',
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Upload'),
  );
  return $form;
}

/**
 * Submit handler for upoad file form.
 * @param $form
 * @param $form_state
 */
function dbslicer_upload_form_submit($form, &$form_state) {
  $id = $form_state['values']['id'];
  $dest = file_directory_path() . '/dbslicer';
  if (file_check_directory($dest, TRUE)) {
    if ($file = file_save_upload('file_upload', array(), $dest)) {
      file_set_status($file, FILE_STATUS_PERMANENT);
      drupal_set_message(t('File saved @file.', array('@file' => $file->filename)));
    }
    else {
      drupal_set_message(t("We couldn't save the file."), 'error');
    }
  }
}