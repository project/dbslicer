<?php

/**
 * @file
 * UI for database slicing.
 */

include_once 'dbslicer_database_slicing.php';
include_once 'dbslicer_export.php';

/**
 * Form for select a starting point for slicing.
 * @param $form_state
 */
function dbslicer_slice_form($form_state) {
  $form = array();
  ahah_helper_register($form, $form_state);

  if (!isset($form_state['storage']['slice_type'])) {
    $slice_type_default = 'static';
  }
  else {
    $slice_type_default = $form_state['storage']['slice_type'];
  }
  $form['slice_type'] = array(
    '#type' => 'radios',
    '#title' => t('Select slicing type'),
    '#options' => array(
      'static' => t('Static'),
      'dynamic' => t('Dynamic'),
    ),
    '#default_value' => $slice_type_default,
    '#ahah' => array(
      'event' => 'change',
      'path' => ahah_helper_path(array('starting_point')),
      'wrapper' => 'starting-point-wrapper',
    ),
  );
  $form['direction'] = array(
    '#type' => 'radios',
    '#title' => t('Direction of slicing'),
    '#options' => array(
      FS => t('Forward slicing'),
      BFS => t('Backward + Forward slicing'),
    ),
    '#default_value' => FS,
  );
  $form['starting_point'] = array(
    '#type' => 'markup',
    '#value' => '<h3>' . t('Select starting table') . '</h3>',
    '#prefix' => '<div id="starting-point-wrapper">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );
  $form['starting_point']['update_sps'] = array(
    '#type' => 'submit',
    '#value' => t('Update starting points'),
    '#submit' => array('ahah_helper_generic_submit'),
    '#attributes' => array('class' => 'no-js'),
  );
  $tables = dbslicer_get_table_definitions();
  $tables = array_keys($tables);
  if ($slice_type_default == 'static') {
    unset($form['starting_point']['start_table']);
    unset($form['starting_point']['starting_row']);
    $tables = dbslicer_get_table_definitions();
    $tables = array_keys($tables);
    $tables = array_combine($tables, $tables);
    ksort($tables);
    $form['starting_point']['table'] = array(
      '#type' => 'radios',
      '#options' => $tables,
      '#prefix' => '<div style="width:100%;height:150px;overflow:auto;">',
      '#suffix' => '</div>',
      '#default_value' => $form_state['storage']['starting_point']['table'],
    );
  }
  else {
    unset($form['starting_point']['table']);
    $tables = dbslicer_get_table_definitions();
    $tables = array_keys($tables);
    $tables = array_combine($tables, $tables);
    ksort($tables);
    if ($form_state['storage']['starting_point']['start_table'] == '') {
      $start_table = 'node';
    }
    else {
      $start_table = $form_state['storage']['starting_point']['start_table'];
    }
    $form['starting_point']['start_table'] = array(
      '#type' => 'radios',
      '#options' => $tables,
      '#default_value' => $start_table,
      '#prefix' => '<div style="width:100%;height:150px;overflow:auto;">',
      '#suffix' => '</div>',
      '#ahah' => array(
        'event' => 'change',
        'path' => ahah_helper_path(array('starting_point', 'starting_row')),
        'wrapper' => 'starting-row-wrapper',
      ),
    );
    $form['starting_point']['newline'] = array(
      '#title' => 'markup',
      '#value' => '<br />' . '<h3>' . t('Select starting row') . '</h3>',
    );
    $form['starting_point']['starting_row'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="starting-row-wrapper" style="width:100%;height:350px;overflow:auto;">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    );
    $data = dbslicer_db_load($start_table);
    $form_state['storage']['data'] = $data;
    foreach ($data as $d) {
      $headers = array();
      $rows = array();
      $row = array();
      foreach ($d as $column => $value) {
        $headers[] = $column;
        if (!is_numeric($value) && drupal_strlen($value) != 0 && drupal_strlen($value) > 10) {
          $title = $value;
          $value = drupal_substr($value, 0, 10) . '...';
          $value = array('data' => $value, 'title' => $title);
        }
        $row['data'][] = $value;
      }
      $rows[] = $row;
      $suffix = '<div>' . theme_table($headers, $rows) . '</div><hr />';
      $form['starting_point']['starting_row'][] = array(
        '#type' => 'checkbox',
        '#suffix' => $suffix,
        '#attributes' => array(
          'style' => 'float:left'
        ),
      );
    }
    $form['starting_point']['starting_row']['update_start_row'] = array(
      '#type' => 'submit',
      '#value' => t('Update starting rows'),
      '#submit' => array('ahah_helper_generic_submit'),
      '#attributes' => array('class' => 'no-js'),
    );
  }

  $form['distance_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Distance limit of slicing'),
    '#default_value' => -1,
    '#description' => t('Negative value will be infinite.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Start slice'),
  );
  return $form;
}

/**
 * Validate handler for slicing form.
 */
function dbslicer_slice_form_validate($form, &$form_state) {
  if ($form_state['values']['slice_type'] == 'dynamic') {
    $checked = 0;
    foreach ($form_state['values']['starting_point']['starting_row'] as $key => $value) {
      if (is_numeric($key) && $value == 1) {
        $checked++;
      }
    }
    if ($checked != 1) {
      drupal_set_message(t('Zero ore more than one row has been selected!'), 'error');
      drupal_goto('dbslicer/slice');
    }
  }
  else {
    if ($form_state['values']['starting_point']['table'] == '') {
      drupal_set_message(t('No table has been selected!'), 'error');
      drupal_goto('dbslicer/slice');
    }
  }

}

/**
 * Submit handler for slicing form.
 * @param $form
 * @param $form_state
 */
function dbslicer_slice_form_submit($form, &$form_state) {
  if (!dbslicer_check_record_identifiers()) {
    drupal_set_message(t("Some table's record identifiers are missing! You must set them first!"), 'error');
    drupal_goto('dbslicer/recordidentifiers');
  }
  $direction = $form_state['values']['direction'];
  $distance = $form_state['values']['distance_limit'];
  if ($form_state['values']['slice_type'] == 'dynamic') {
    $table = $form_state['values']['starting_point']['start_table'];
    $data = $form_state['storage']['data'];
    foreach ($form_state['values']['starting_point']['starting_row'] as $key => $value) {
      if (is_numeric($key) && $value == 1) {
        $data = $data[$key];
        break;
      }
    }
    $SPS = array(
      'table' => $table,
      'column' => $data,
    );
    $RDG = dbslicer_generate_RDG();
    $ri = $RDG['record identifiers'][$SPS['table']];
    foreach ($RDG['connections'][$SPS['table']] as $c) {
      $name = dbslicer_ntoc($c);
      if (!in_array($name, $ri)) {
        unset($SPS['column'][$name]);
      }
    }
    dbslicer_dynamic_slice($RDG, $SPS, $direction, $distance);
  }
  else {
    $table = $form_state['values']['starting_point']['table'];
    $link_to_file = dbslicer_export_static($table, $direction, $distance);
    drupal_set_message(t('Database slice generated:'));
    drupal_set_message(t('You can download it !here.', array('!here' => l(t('here'), $link_to_file))));
    drupal_goto('dbslicer/manage');
  }
}