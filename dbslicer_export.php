<?php

/**
 * @file
 * Export the result of a database slice in XML format.
 */

include_once 'dbslicer_database_slicing.php';

/* Types fo graphs */
define("TDGraph", 1);
define("RDGraph", 2);

/**
 * Convert a graph to schema XML.
 * Because the Record and Table Dependence Graph have very similat structure
 * we can convert them in one function.
 * @param array $G
 *  Graph we want to compare.
 * @param $graph_type
 *  Type of graph. Could be TDGraph or RDGraph.
 * @return DOMDocument
 *  Schema document in XML format.
 */
function dbslicer_graph_to_xml(array $G, $graph_type) {
  $doc = new DOMDocument('1.0', 'UTF-8');
  $doc->formatOutput = TRUE;
  //Select the name of the edges between columns and tables.
  $adj = 'adj';
  if (RDGraph == $graph_type) {
    $adj = 'connections';
  }
  // Root element.
  $database = $doc->createElement('database');
  $doc->appendChild($database);
  if (TDGraph == $graph_type) {
    $tables = $G['tables'];
  }
  else {
    $tables = array_keys($G['tables']);
  }
  // Process all tables.
  foreach ($tables as $t) {
    $table = $doc->createElement("table");
    $name = $doc->createElement('name', $t);
    $table->appendChild($name);
    // Process a table's columns.
    foreach ($G[$adj][$t] as $c) {
      $column = $doc->createElement('column');
      $name = $doc->createElement('name', dbslicer_ntoc($c));
      $column->appendChild($name);
      $type = $doc->createElement('type', $G['type map'][$c]);
      $column->appendChild($type);
      if (in_array($c, $G['primary keys'][$t])) {
        $primary = $doc->createElement('primary');
        $column->appendChild($primary);
      }
      if (in_array(dbslicer_ntoc($c), $G['record identifiers'][$t])) {
        $primary = $doc->createElement('identifier');
        $column->appendChild($primary);
      }
      // If the column is foreign key, an edge will start from here, so it has
      // adjacent node. Every column has only one adjacent table.
      if (isset($G[$adj][$c])) {
        if (TDGraph == $graph_type) {
          $foreign_table = $G[$adj][$c][0][0];
          $foreign_column =  $G[$adj][$c][0][1];
        }
        else {
          $foreign_table = dbslicer_ntot($G[$adj][$c][0]);
          $foreign_column = dbslicer_ntoc($G[$adj][$c][0]);
        }
        $reference = $doc->createElement('reference');
        $reference->setAttribute('table', $foreign_table);
        $reference->setAttribute('column', dbslicer_ntoc($foreign_column));
        $column->appendchild($reference);
      }
      $table->appendChild($column);
    }
    $database->appendChild($table);
  }
  return $doc;
}

/**
 * Recursive function to load the record identifiers into a table's foreign key.
 *
 * @param DOMNode $column
 *  Column node. This is the foreign key where we load the references to.
 * @param XMLWriter $writer
 *  XML writer object.
 * @param DOMXPath $xpath
 *  Xpath object of the schema.
 * @param array $data
 *  The row's associative array we are processing now.
 * @param string $type
 *  Type of foreign key.
 */
function dbslicer_export_load_references(DOMNode $column, XMLWriter &$writer, DOMXPath $xpath, $data, $type) {
  $foreign_table = $xpath->query("reference/@table", $column)->item(0)->textContent;
  $foreign_column = $xpath->query("reference/@column", $column)->item(0)->textContent;
  // Get the identifiers of referenced tables.
  $identifiers = $xpath->query("/database/table[name='$foreign_table']/column[identifier]");

  $writer->startElement($foreign_table);
  //load data
  if ($type == 'text') {
    $data = '^' . $data . '$';
  }
  $where = array(
    $foreign_column => $data
  );
  // Load the referenced row.
  $foreign_data = dbslicer_db_load($foreign_table, $where);
  if (count($foreign_data) != 0) {
    $foreign_data = array_shift($foreign_data);
    foreach ($identifiers as $id) {
      $id_name = $xpath->query("name", $id)->item(0)->textContent;
      $id_type = $xpath->query("type", $id)->item(0)->textContent;
      $writer->startElement($id_name);
      $isReference = $xpath->query("reference", $id)->length;
      // If the identifier is a reference, call this function again.
      if ($isReference) {
        dbslicer_export_load_references($id, $writer, $xpath, $foreign_data[$id_name], $id_type);
      }
      else {
        $writer->text($foreign_data[$id_name]);
      }
      $writer->endElement(); // id_name
    }
  }
  $writer->endElement(); //foreign table
}

/**
 * Export a static slice result.
 * @param string $SPS
 *  Starting Point of Slicing. Name of the table.
 * @param int $direction
 *  Direction of slicing. Could be FS or BFS.
 * @param int $distance
 *  Distance limit of slicing. Negative value means infinite distance.
 * The result will be written into the files directory.
 * The file name will be static_export_date.xml
 */
function dbslicer_export_static($SPS, $direction = FS, $distance = -1) {
  $TDG = dbslicer_generate_TDG();
  $schema_doc = dbslicer_graph_to_xml($TDG, TDGraph);
  $xpath = new DOMXPath($schema_doc);
  $result = dbslicer_static_slice($TDG, $SPS, $direction, $distance);
  $writer = new XMLWriter();
  $dest = file_directory_path() . '/dbslicer';
  file_check_directory($dest, TRUE);
  $link_to_file = $dest . '/static_export_' . date('Y_m_d_H_i') . '.xml';
  // Preprocess schema.
  $doc = new DOMDocument('1.0', 'UTF-8');
  $doc->formatOutput = TRUE;
  $database = $doc->createElement('database');
  $doc->appendChild($database);
  /*
   * Get the tables from result. We just write out the schema of tables that
   * take part in slicing result.
   */
  foreach ($result as $table) {
    $table_node = $xpath->query("/database/table[name='$table']")->item(0);
    $table_node = $doc->importNode($table_node, TRUE);
    $database->appendChild($table_node);
  }
  $doc_xpath = new DOMXPath($doc);
  $writer->openURI($link_to_file);
  $writer->startDocument('1.0', 'UTF-8');
  $writer->setIndent(4);
  $writer->startElement('dbslicer');
  $writer->writeRaw("\n " . $doc->saveXML($doc_xpath->query("/database")->item(0)) . "\n");
  $writer->startElement('content');
  foreach ($result as $table) {
    $writer->startElement($table);
    // Get all rows from table.
    $rows = dbslicer_db_load($table);
    $query = "/database/table[name='$table']/column";
    $columns = $xpath->query($query);
    foreach ($rows as $r) {
      $writer->startElement('row');
      // Process all columns of row
      foreach ($columns as $c) {
        $name = $xpath->query("name", $c)->item(0)->textContent;
        $type = $xpath->query("type", $c)->item(0)->textContent;
        if ($type == 'indexer') {
          continue;
        }
        $writer->startElement($name);
        $isPrimary = $xpath->query("primary", $c)->length;
        $isReference = $xpath->query("reference", $c)->length;
        $isIdentifier = $xpath->query("identifier", $c)->length;
        // If the column is a reference, load the reference's record identifiers.
        if ($isReference) {
          dbslicer_export_load_references($c, $writer, $xpath, $r[$name], $type);
        }
        else {
          $writer->text($r[$name]);
        }
        // End of name.
        $writer->endElement();
      }
      // End of row.
      $writer->endElement();
    }
    // End of table.
    $writer->endElement();
  }
  // End of content.
  $writer->endElement();
  // End of dbslicer.
  $writer->endElement();
  $writer->flush();
  return $link_to_file;
}

function dbslicer_export_dynamic(array $RDG, $result) {

  $schema_doc = dbslicer_graph_to_xml($RDG, RDGraph);
  $xpath = new DOMXPath($schema_doc);
  // Sort the result array by keys, so the rows from one table will be grouped together.
  ksort($result);

  $tables = array();
  foreach (array_keys($result) as $row_name) {
    $t = dbslicer_ntot($row_name);
    $tables[$t] = $t;
  }

  $doc = new DOMDocument('1.0', 'UTF-8');
  $doc->formatOutput = TRUE;
  $database = $doc->createElement('database');
  $doc->appendChild($database);
  // Get the tables from result. We just write out the schema of tables that
  // take part in slicing result.
  foreach ($tables as $t) {
    $table_node = $xpath->query("/database/table[name='$t']")->item(0);
    $table_node = $doc->importNode($table_node, TRUE);
    $database->appendChild($table_node);
  }
  $doc_xpath = new DOMXPath($doc);

  $writer = new XMLWriter();
  $dest = file_directory_path() . '/dbslicer';
  file_check_directory($dest, TRUE);
  $link_to_file = $dest . '/dynamic_export_' . date('Y_m_d_H_i') . '.xml';
  $writer->openURI($link_to_file);
  $writer->startDocument('1.0', 'UTF-8');
  $writer->setIndent(4);
  $writer->startElement('dbslicer');
  $writer->writeRaw("\n " . $doc->saveXML($doc_xpath->query("/database")->item(0)) . "\n");
  $writer->startElement('content');
  // Used for notice table changes.
  $prev_table = '';
  foreach ($result as $row_namngee => $row) {
    $table = dbslicer_ntot($row_name);
    if ($table != $prev_table) {
      if ($prev_table != '') {
        // End of previous table.
        $writer->endElement();
      }
      $writer->startElement($table);
    }
    $writer->startElement('row');
    $query = "/database/table[name='$table']/column";
    $columns = $xpath->query($query);
    // process all columns
    foreach ($columns as $c) {
      $name = $xpath->query("name", $c)->item(0)->textContent;
      $type = $xpath->query("type", $c)->item(0)->textContent;
      // we left out indexing primary keys from the exported files
      if ($type == 'indexer') {
        continue;
      }
      $writer->startElement($name);
      $isPrimary = $xpath->query("primary", $c)->length;
      $isReference = $xpath->query("reference", $c)->length;
      $isIdentifier = $xpath->query("identifier", $c)->length;
      // if the column is a reference, load its record identifiers
      if ($isReference) {
        dbslicer_export_load_references($c, $writer, $xpath, $row[$name], $type);
      }
      else {
        $writer->text($row[$name]);
      }
      // End of name.
      $writer->endElement();
    }
    // End of row.
    $writer->endElement();
    $prev_table = $table;
  }
  // End of last table.
  $writer->endElement();
  // End of content.
  $writer->endElement();
  // End of dbslicer.
  $writer->endElement();
  $writer->flush();
  return $link_to_file;
}
